# Các câu hỏi phỏng vấn về Html-Css cho Frontend Developer

* Selectors
    * Độ ưu tiên khi select DOM element.
       * `id > class > tag`
    * Pseudo-class là gì:
        * `:link, :hover, :active, :first, :last...`
    * Child selectors, phân biệt:
        *  `div, p  :   Selects all <div> elements and all <p> elements.`
        *  `div p   :	Selects all <p> elements inside <div> elements.	`
        *  `div > p :   Selects all <p> elements where the parent is a <div> element.`	
        *  `div+p   :   Selects all <p> elements that are placed immediately after <div> element`
        *  `p~ul    :   Selects every <ul> element that are preceded(đứng trước) by a <p> element`
        *  `[title~=test] Selects all elements with a title attribute containing the word "test"`
        *  `[lang|=en] Selects all elements with a lang attribute value starting with "en"`
        *  `[class^="test"] Selects every <a> element whose class value begins with "test"`
        *  `[class$="test"] Selects every <a> element whose class value ends with "test"`
        *  `[class*="test"] Selects every <a> element whose class value contains with "test"`
        
* Pseudo-elements
    * `::before, ::after`
* Z-index
* Positioning elements
    * Phân biệt position relative, absolute, fixed, static
        * `relative: tương đối -> đối chiếu vs chính nó không thay đổi vị trí ban đầu`
        * `absolute: căn vị trí đối chiếu vs thành phần cha của nó`
        * `fixed: căn vị trí đối chiếu vs cửa sổ trình duyệt.`
        * `static: hiển thị mặc định`
    * Phân biệt cách sử dụng float và display:inline-block
        * `display: inline-block thường sử dụng cho các văn bản nội tuyến nhưng cho phép có thuộc tính của cấp độ khối.`
        * `float: dùng cho các khối nội dung`
        * Cách fix cho inline-block nếu chiều cao hai khối nội dung không bằng nhau
            * ` dùng vertical-align`
        * Cách fix cho float
            * ` clear: left, clear: right`
* Khác biệt giữa visibility:hidden and display:none
    * `visibility:hidden chỉ ẩn đi và vẫn tốn không gian còn display:none là ẩn hoàn toàn`
* Ưu điểm/nhược điểm của việc sử dụng CSS preprocessors?
    * `Ưu: Cho phép sử dụng các cú pháp lồng nhau. Định nghĩa các biến và hàm, sử dụng công thức tính toán
    join nhiều files lại với nhau`
    * `Nhuoc: Khó trong việc theo dõi dung lượng, khó khăn trong việc bảo trì, cập nhật và sửa lỗi.`
* Có sử dụng tool validate HTML không?
* Nên sử dụng bao nhiêu tag H1 ? Vì sao?
    * `1 => related SEO performance`
* Nên sử dụng định dạng video nào nếu cần nhúng vào website
    * ` webm hoặc ogv`
* Ưu / nhược điểm của việc sử dụng CSS Framework
    * ` Ưu: nhanh chóng, thuận tiện, tối ưu hóa`
    * ` Nhược: Quá nhiều feature không cần thiết nếu không sử dụng hết và đôi khi cần overrides lại nhiều`
* Responsive
    * meta-viewport là gì?
        `meta viewport nghĩa là một thẻ thiết lập cho trình duyệt hiển thị tương ứng với kích thước màn hình. <meta name="viewport" content="width=device-width, initial-scale=1"> có nghĩa là bạn sẽ thiết lập trình duyệt hiển thị cố định và tương ứng trên tất cả các thiết bị dựa vào chiều rộng của thiết bị (device-width) và không cho phép người dùng phóng to (thiết lập initial-scale với giá trị cố định là 1). Đây là thẻ mình khuyến khích bạn sử dụng cho toàn bộ các dự án Responsive.`
    * Nên sử dụng đơn vị tương đối hay tuyệt đối (`px,pt vs em,rem,%`) cho responsive website
        * ` tương đối cho lay out và tuyệt đối cho typography`
    * Phân biệt Mobile First & Desktop First ? Ưu/nhược
        * `Mobile First là một phương pháp tạo mã CSS đáp ứng, nơi mà các kiểu thiết bị di động được viết trước, không có truy vấn phương tiện truyền thông và cho mỗi bước kích thước màn hình, một truy vấn phương tiện mới được thêm vào mở rộng, sửa đổi và ghi đè các kiểu hiện có của Nhỏ hơn).`
           
        *  `Ưu điểm - Dễ dàng tối ưu hóa thời gian tải trên các thiết bị nhỏ hơn vì không có tài sản và kiểu dáng bổ sung (tài sản được liên kết trong các truy vấn phương tiện sau không tải). Rất hiệu quả cho các thiết kế đơn giản hơn, ở đó các kiểu màn hình lớn hơn là một phiên bản nâng cấp của các mẫu nhỏ hơn, ví dụ như khi trang trí hoặc các yếu tố mới được thêm vào để có trải nghiệm phong phú hơn trên màn hình lớn hơn.
           Nhược điểm - Kỹ thuật này không hiệu quả nếu các phiên bản di động và máy tính để bàn của bố cục có sự khác biệt đáng kể hoặc cho các bố cục phức tạp và cho hầu hết các ứng dụng web. Trong những trường hợp như vậy, mã Mobile First phức tạp hơn mức cần thiết với quá nhiều ghi đè.`
        *  `Desktop Đầu tiên là cách cổ điển để viết CSS; Phiên bản dành cho máy tính để bàn của các kiểu này không nằm trong các truy vấn phương tiện truyền thông hoặc bắt đầu với yêu cầu về chiều rộng tối thiểu và cho mỗi kích thước màn hình nhỏ hơn so với phiên bản ban đầu, một truy vấn phương tiện mới được thêm vào bắt đầu từ lớn nhất đến nhỏ nhất. Với từng truy vấn truyền kết quả, các phần tử bị ẩn, sắp xếp lại hoặc sắp xếp lại.`
           
        *  `Ưu điểm - Trái với phương pháp Mobile First, Desktop First xuất sắc với thiết kế khác biệt đáng kể giữa các kích cỡ màn hình, chẳng hạn như khi các phần tử bị xóa khi màn hình nhỏ hơn. Đây cũng là cách duy nhất để thêm khả năng đáp ứng cho một trang web cũ hơn hiện tại mà không cần viết lại mã của nó.
           Nhược điểm - Nếu không được triển khai chính xác, có thể dẫn đến mã không được sử dụng hoặc bị ghi đè và các tài sản được tải trên các thiết bị nhỏ hơn, do đó làm cho trang web nặng hơn trên điện thoại thông minh. Điều này dẫn đến việc loại bỏ các kiểu không cần thiết và viết mã bổ sung cho các thiết kế đơn giản hơn, nơi Mobile First có thể được thực hiện dễ dàng hơn.
           Đối với các trang web nơi phiên bản dành cho thiết bị di động và máy tính để bàn hơi khác nhau, thường là cách thức tốt hơn để phân phối các trang định dạng riêng biệt dựa trên kích thước màn hình hoặc cô lập hai bố cục trong các truy vấn phương tiện không chồng lấn, mỗi điểm đặc biệt mạnh mẽ cho các thiết bị mục tiêu của nó.`
        * Một số kiến thức cơ bản khi viết CSS Responsive
            * `Ngoài đơn vị của breakpoint là px, thì các đơn vị đo chiều dài trong website nên là phần trăm. Hay nói đúng hơn là sử dụng đơn vị tương đối.`
            * `Nên sử dụng max-width thay vì width để tránh cố định chiều rộng.`
            * `Sử dụng display: none cho các thành phần cần ẩn đi ở từng thiết bị mà bạn muốn ẩn. Và display: block ở các thiết bị cần hiển thị ra.`
            * `Sử dụng tùy chọn !important nếu cần đè viết đè CSS.`
* Perfomance
    * Nêu một số cách optimize website
        * `minimize CSS & JS. Service side caching. Serve resposive images according device screen size and only load 
        for user's case`
        * `Write clean html, dont include complied frameworks, stop loading unused scripts or CSS modules.`